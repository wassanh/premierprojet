from tp2 import *

def test_box_create():
    b = Box()

def test_box_add():
    b = Box()
    b.add("truc1")
    b.add("truc2")

def test_box_in():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    b.add("truc3")

    assert "truc1" in b 
    assert "truc2" in b 
    assert "truc3" in b 

def test_box_remove():
    b = Box()
    b.add("truc")
    b.remove("truc")

    assert "truc" not in b 

def test_box_open():
    b = Box()

    b.open()
    assert b.is_open()

    b.close()
    assert not b.is_open()

def test_box_look():
    b = Box()
    b.add("ceci")
    b.add("cela")

    b.open()
    assert b.action_look() == "la boite contient : ceci, cela"

    b.close()
    assert b.action_look() == "la boite est fermee"


def test_thing_create():
    t = thing(3)
    assert t.volume() == 3

def test_box_capacity():
    b = Box()
    assert b.capacity() is None 

    b.set_capacity(5)
    assert b.capacity() == 5

def test_has_room_for():
    b = Box()
    t = thing(3)


    assert b.test_has_room_for(t)

    b.set_capacity(3)
    assert b.test_has_room_for(t)

    b.set_capacity(0)
    assert b.test_has_room_for(t)
